import react.FC
import react.Fragment
import react.dom.html.ReactHTML
import react.useState
import web.html.ButtonType

val UnoptimizedCounter = FC("UnoptimizedCounter") {
    val (count, setCount) = useState(0)

    Fragment {
        ReactHTML.p {
            +"The count is: "
            +count.toString()
        }
        ReactHTML.button {
            type = ButtonType.button
            //FIXME: Avoid expensive DOM changes with useCallback
            onClick = { _: Any -> setCount(count + 1) }
            +"Increase"
        }
    }
}
