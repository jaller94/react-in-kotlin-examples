import react.FC
import react.dom.html.ReactHTML

private val recentReads = listOf(
    Book(1, "Show Your Work!", "Austin Kleon"),
    Book(2, "How to Be Everything", "Emilie Wapnick")
)

val RecentReadsArray = FC("RecentReadsArray") {
    ReactHTML.aside {
        ReactHTML.h2 {
            +"Recent Reads"
        }
        ReactHTML.ol {
            for (recentRead in recentReads) {
                BookItem {
                    key = recentRead.id.toString()
                    book = recentRead
                }
            }
        }
    }
}
