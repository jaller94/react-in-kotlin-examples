import mui.material.Typography
import mui.material.styles.TypographyVariant
import mui.system.sx
import react.FC
import react.dom.html.ReactHTML
import web.cssom.Overflow
import web.cssom.TextOverflow
import web.cssom.WhiteSpace
import web.cssom.number

val OpeningHoursNotice = FC("OpeningHoursNotice") {
    Typography {
        component = ReactHTML.div
        variant = TypographyVariant.h6
        sx {
            flexGrow = number(1.0)
            overflow = Overflow.hidden
            textOverflow = TextOverflow.ellipsis
            whiteSpace = WhiteSpace.nowrap
        }
        +"The library closes at 10 PM."
    }
}
