import react.FC
import react.dom.html.ReactHTML

val HelloWorld = FC("HelloWorld") {
    ReactHTML.h1 {
        +"Hello World!"
    }
}
