import react.FC
import react.Fragment
import react.dom.events.MouseEvent
import react.dom.html.ReactHTML
import react.useCallback
import react.useState
import web.html.ButtonType
import web.html.HTMLButtonElement

val Counter = FC("Counter") {
    val (count, setCount) = useState(0)

    val handleIncrease = useCallback<(it: MouseEvent<HTMLButtonElement, *>) -> Unit> {
        setCount { value -> value + 1 }
    }

    Fragment {
        ReactHTML.p {
            +"The count is: "
            +count.toString()
        }
        ReactHTML.button {
            type = ButtonType.button
            onClick = handleIncrease
            +"Increase"
        }
    }
}
