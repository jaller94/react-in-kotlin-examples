import react.FC
import react.dom.events.ChangeEventHandler
import react.dom.events.FormEventHandler
import react.dom.html.ReactHTML
import react.useCallback
import react.useState
import sendBookOrder
import web.html.ButtonType
import web.html.HTMLFormElement
import web.html.HTMLInputElement

val BookOrderForm = FC("BookOrderForm") { props ->
    val (title, setTitle) = useState("")

    val handleSubmit = useCallback<FormEventHandler<HTMLFormElement>> { event ->
        event.preventDefault()

        sendBookOrder(title)
    }

    val handleTitleChange = useCallback<ChangeEventHandler<HTMLInputElement>> { event ->
        setTitle(event.target.value)
    }

    ReactHTML.form {
        onSubmit = handleSubmit
        ReactHTML.h2 {
            +"Order Service"
        }
        ReactHTML.label {
            +"Book title:"
            ReactHTML.input {
                value = title
                onChange = handleTitleChange
            }
        }
        ReactHTML.button {
            type = ButtonType.submit
            +"Order"
        }
    }
}
