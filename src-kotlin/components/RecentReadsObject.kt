import react.FC
import react.dom.html.ReactHTML

private val recentReads: Map<String, Book> = mapOf(
    "fettnäpfchenführer-taiwan" to Book(4, "Fettnäpfchenführer Taiwan", "Deike Lautenschläger")
)

val RecentReadsObject = FC("RecentReadsObject") {
    ReactHTML.aside {
        ReactHTML.h2 {
            +"Recent Reads"
        }
        ReactHTML.ol {
            for ((bookId, recentRead) in recentReads) {
                BookItem {
                    key = bookId.toString()
                    book = recentRead
                }
            }
        }
    }
}
