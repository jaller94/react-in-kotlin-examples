import react.FC
import react.dom.html.ReactHTML

val Outro = FC("Outro") {
    ReactHTML.span {
        asDynamic()["test-id"] = "outro"
        +"Thanks for reading!"
    }
}
