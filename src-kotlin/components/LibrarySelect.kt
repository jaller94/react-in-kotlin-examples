import react.FC
import react.Fragment
import react.dom.events.ChangeEventHandler
import react.dom.html.ReactHTML
import react.useCallback
import react.useState
import web.html.HTMLSelectElement

val LibrarySelect = FC("LibrarySelect") {
    val (library, setLibrary) = useState("ksml-main")

    val handleChange: ChangeEventHandler<HTMLSelectElement> = useCallback { event ->
        setLibrary(event.target.value)
    }

    Fragment {
        ReactHTML.label {
            +"Library:"
            ReactHTML.select {
                value = library
                onChange = handleChange
                ReactHTML.optgroup {
                    label = "Kaohsiung"
                    ReactHTML.option {
                        value = "ksml-main"
                        +"KSML Main Branch"
                    }
                    ReactHTML.option {
                        value = "ksml-gushan"
                        +"KSML Gushan Branch"
                    }
                }
                ReactHTML.optgroup {
                    label = "Vancouver"
                    ReactHTML.option {
                        value = "vpl-central"
                        +"VPL Central Library"
                    }
                    ReactHTML.option {
                        value = "vpl-hastings"
                        +"VPL Hastings Branch"
                    }
                }
            }
        }
    }
}
