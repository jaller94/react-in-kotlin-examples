import react.FC
import react.dom.html.ReactHTML

private val recentReads: Map<Int, Book> = mapOf(
    4 to Book(4, "Der letzte Außenposten", "Anne Polifka")
)

val RecentReadsMap = FC("RecentReadsMap") {
    ReactHTML.aside {
        ReactHTML.h2 {
            +"Recent Reads"
        }
        ReactHTML.ol {
            for ((bookId, recentRead) in recentReads) {
                BookItem {
                    key = bookId.toString()
                    book = recentRead
                }
            }
        }
    }
}
