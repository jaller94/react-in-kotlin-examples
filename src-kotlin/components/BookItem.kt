import react.FC
import react.Props
import react.dom.html.ReactHTML

data class Book(
    val id: Int,
    val title: String,
    val author: String
)

external interface BookItemProps : Props {
    var book: Book
}

val BookItem = FC<BookItemProps>("BookItem") { props ->
    ReactHTML.li {
        +props.book.author
        +" - "
        +props.book.title
    }
}
