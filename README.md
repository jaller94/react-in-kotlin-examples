# React in Kotlin examples

These are the code files for the blog post "React in Kotlin".

https://chrpaul.de/posts/2023-12-29-react-in-kotlin/

## Build

The web app with bundled JavaScript libraries will be put into the folder `out`.

```bash
bun install
bun run build
```
