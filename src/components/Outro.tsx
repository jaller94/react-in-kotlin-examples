import React from 'react';

export function Outro(): React.JSX.Element {
    return (
        <h1
            data-test-id="outro"
        >Thanks for reading!</h1>
    );
}
