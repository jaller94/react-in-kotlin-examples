import React from 'react';
import { Book, BookItem } from './BookItem';

const recentReads: Book[] = [
    {
        id: 1,
        title: 'Show Your Work!',
        author: 'Austin Kleon',
    },
    {
        id: 2,
        title: 'How to Be Everything',
        author: 'Emilie Wapnick',
    },
];

export function RecentReadsArray(): React.JSX.Element {
    return (
        <aside>
            <h2>Recent Reads</h2>
            <ol>
                {recentReads.map(book => (
                    <BookItem
                        key={book.id}
                        book={book}
                    />
                ))}
            </ol>
        </aside>
    );
}