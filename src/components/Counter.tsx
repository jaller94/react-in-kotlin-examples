import React, { Fragment, useCallback, useState } from 'react';

export function Counter(): React.JSX.Element {
    const [count, setCount] = useState<number>(0);

    const handleIncrease = useCallback(() => {
        // This uses the callback variant of setState.
        setCount(value => value + 1);
    }, []);

    return (
        <Fragment>
            <p>The count is: {count}</p>
            <button
                type="button"
                onClick={handleIncrease}
            >Increase</button>
        </Fragment>
    );
}
