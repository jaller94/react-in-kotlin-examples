import React, { useCallback, useState } from 'react';
import { sendBookOrder } from '../api.ts';

export function LibraryApp(): React.JSX.Element {
    const [ bookTitle, setBookTitle ] = useState<string>('');

    //FIXME: Avoid expensive DOM changes by reading the input values from the event
    const handleSubmit: React.FormEventHandler<HTMLFormElement> = useCallback(event => {
        // We don't want the browser to handle this and possibly reload the page.
        event.preventDefault();
        
        const elements = new FormData(e.currentTarget);
        const bookTitle = elements.get('book-title');

        sendBookOrder(bookTitle);
    }, [bookTitle]);

    const handleBookTitleChange: React.ChangeEventHandler<HTMLInputElement> = useCallback(event => {
        setBookTitle(event.target.value);
    }, []);

    return (
        <form
            onSubmit={handleSubmit}
        >
            <h1>Library App</h1>
            <label>
                Book title:
                <input
                    name="book-title"
                    value={bookTitle}
                    onChange={handleBookTitleChange}
                />
            </label>
            <button type="submit">Order</button>
        </form>
    );
}
