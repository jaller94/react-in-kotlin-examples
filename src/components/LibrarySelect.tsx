import React, { useCallback, useState } from 'react';

export function BranchSelect(): React.JSX.Element {
    const [branch, setBranch] = useState<string>('');

    const handleChange: React.ChangeEventHandler<HTMLSelectElement> = useCallback(event => {
        setBranch(event.target.value);
    }, []);

    return (
        <label>
            Branch:
            <select
                value={branch}
                onChange={handleChange}
            >
                <optgroup label="Kaohsiung">
                    <option value="ksml-main">KSML Main Branch</option>
                    <option value="ksml-gushan">KSML Gushan Branch</option>
                </optgroup>
                <optgroup label="Vancouver">
                    <option value="vpl-central">VPL Central Library</option>
                    <option value="vpl-hastings">VPL Hastings Branch</option>
                </optgroup>
            </select>
        </label>
    );
}
