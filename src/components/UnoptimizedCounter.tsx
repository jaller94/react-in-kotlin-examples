import React, { Fragment, useState } from 'react';

export function UnoptimizedCounter(): React.JSX.Element {
    const [count, setCount] = useState<number>(0);

    //FIXME: Avoid expensive DOM changes with useCallback
    return (
        <Fragment>
            <p>The count is: {count}</p>
            <button
                type="button"
                onClick={() => setCount(count + 1)}
            >Increase</button>
        </Fragment>
    );
}
