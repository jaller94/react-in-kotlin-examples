import React, { useCallback, useState } from 'react';
import { sendBookOrder } from '../api.ts';

export function BookOrderForm(): React.JSX.Element {
    const [title, setTitle] = useState<string>('');

    //FIXME: Avoid expensive DOM changes by reading the input values from the event
    const handleSubmit: React.FormEventHandler<HTMLFormElement> = useCallback(event => {
        // We don't want the browser to handle this and possibly reload the page.
        event.preventDefault();

        sendBookOrder(title);
    }, [title]);

    const handleTitleChange: React.ChangeEventHandler<HTMLInputElement> = useCallback(
        event => setTitle(event.target.value),
        []
    );

    return (
        <form
            onSubmit={handleSubmit}
        >
            <h2>Order Service</h2>
            <label>
                Book title:
                <input
                    value={title}
                    onChange={handleTitleChange}
                />
            </label>
            <button type="submit">Order</button>
        </form>
    );
}
