import React from 'react';

export type Book = {
    id: number;
    title: string;
    author: string;
};

type BookItemProps = {
    book: Book;
};

export function BookItem(props: BookItemProps): React.JSX.Element {
    return (
        <li>{props.book.author} - {props.book.title}</li>
    );
}
