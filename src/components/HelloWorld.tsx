import React from 'react';

export function HelloWorld(): React.JSX.Element {
    return <h1>Hello world!</h1>;
}
