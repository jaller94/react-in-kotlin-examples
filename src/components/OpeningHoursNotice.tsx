import React from 'react';
import { Typography } from '@mui/material';

export function OpeningHoursNotice(): React.JSX.Element {
    return (
        <Typography
            component="div"
            variant="h6"
            sx={{
                flexGrow: 1,
                overflowX: 'hidden',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
            }}
        >The library closes at 10 PM.</Typography>
    );
}
