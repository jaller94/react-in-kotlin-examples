import React from 'react';
import { Book, BookItem } from './BookItem';

const recentReads: Map<number, Book> = new Map([
    [4, {
        id: 4,
        title: 'Der letzte Außenposten',
        author: 'Anne Polifka',
    }],
]);

export function RecentReadsMap(): React.JSX.Element {
    return (
        <aside>
            <h2>Recent Reads</h2>
            <ol>
                {[...recentReads.entries()].map(([key, book]) => (
                    <BookItem
                        key={key}
                        book={book}
                    />
                ))}
            </ol>
        </aside>
    );
}
