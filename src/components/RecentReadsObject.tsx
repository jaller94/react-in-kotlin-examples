import React from 'react';
import { Book, BookItem } from './BookItem';

const recentReads: Record<string, Book> = {
    'fettnäpfchenführer-taiwan': {
        id: 3,
        title: 'Fettnäpfchenführer Taiwan',
        author: 'Deike Lautenschläger',
    },
};

export function RecentReadsObject(): React.JSX.Element {
    return (
        <aside>
            <h2>Recent Reads</h2>
            <ol>
                {[...Object.entries(recentReads)].map(([key, book]) => (
                    <BookItem
                        key={key}
                        book={book}
                    />
                ))}
            </ol>
        </aside>
    );
}
