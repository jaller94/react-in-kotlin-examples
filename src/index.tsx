import { createRoot } from 'react-dom/client';
import React from 'react';
import { HelloWorld } from './components/HelloWorld';
import { Counter } from './components/Counter';
import { UnoptimizedCounter } from './components/UnoptimizedCounter';
import { OpeningHoursNotice } from './components/OpeningHoursNotice';
import { BookOrderForm } from './components/BookOrderForm';
import { Outro } from './components/Outro';
import { RecentReadsArray } from './components/RecentReadsArray';
import { RecentReadsObject } from './components/RecentReadsObject';
import { RecentReadsMap } from './components/RecentReadsMap';
import { BranchSelect } from './components/LibrarySelect';
const root = createRoot(document.getElementById('root'));
root.render(
    <>
        <HelloWorld/>
        <OpeningHoursNotice/>
        <RecentReadsArray/>
        <RecentReadsObject/>
        <RecentReadsMap/>
        <UnoptimizedCounter/>
        <Counter/>
        <BookOrderForm/>
        <BranchSelect/>
        <Outro/>
    </>
);
